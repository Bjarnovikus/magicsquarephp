<?php

function err($msg)
{
	echo("<p class=\"err\">".$msg."</p>");
}

function not($msg)
{
	echo("<p class=\"not\">".$msg."</p>");
}

function okm($msg)
{
	echo("<p class=\"okm\">".$msg."</p>");
}

//Source: http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
function size_formatted($size)
{
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}
?>