<?php
include("mainfunctions.php");
include("functions.php");

/////////////////////
$limit = 500;
/////////////////////

$starttime = microtime(true);
?>
<!DOCTYPE html>
<html>
<head>
<title>Magisch Vierkant</title>
<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
</head>
<body>

<div id="result">

<?php

	if (!(isset($_GET["method"])))
	{
		err("Er is geen methode opgegeven.");
	}
	else
	{
	
		if (!(isset($_GET["n"])))
		{
			err("Er is geen dimensie opgegeven.");
		}
		else
		{
		
			$n = intval($_GET["n"]);
			$method = $_GET["method"];
			
			
			if ( ($n <= $limit) or ($limit == -1) )
			{
			
				$methodfound = false;
				
				include_once("methodes/methodevanboogmans.php");
				include_once("methodes/methodevandelahire.php");
				include_once("methodes/viervouden_1.php");
				include_once("methodes/viervouden_2.php");
				include_once("methodes/evenmethode.php");
				include_once("methodes/evenmethode_stap1.php");
				include_once("methodes/getall.php");
				
				$checksquare = false;
				
				if (isset($_GET["checksquare"]))
				{
					$checksquare = true;
				}
				
				if ($method == "methodevanboogmans")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = diagonaalmethode($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($method == "methodevandelahire")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = methodevandelahire($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($method == "viervouden_1")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = viervouden1($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($method == "viervouden_2")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = viervouden2($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($method == "evenmethode")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = methodevanstrachey($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($method == "evenmethode_stap1")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = methodevanstrachey_stap1($n);
					tekenvierkant($square);
					if ($checksquare == true) {not("Narekenen is zinloos, aangezien dit slechts het eerste deel is van een methode."); }
				
				}
				
				if ($method == "getall")
				{
				
					$methodfound = true;
					
					calcinfo($method,$n);
					$square = getallsquare($n);
					tekenvierkant($square);
					if ($checksquare == true) {vierkantnakijken($square); }
				
				}
				
				if ($methodfound == false)
				{
					err("De opgevraagde methode kon niet gevonden worden.");
				}
			
			}
			else
			{
				err("Het vooraf vastgelegde limiet van ".$limit." zou overschreden worden als dit vierkant berekend zou worden. De berekening werd niet uitgevoerd.");
			}
		
		}
		
	}

?>

</div>

<div id="calcinfo">
<?php
$endtime = microtime(true);
$elapsedtime = $endtime-$starttime;
echo("Berekeningstijd: ".round($elapsedtime*1000,5)." ms<br>");
echo("Maximum geheugen: ".size_formatted(memory_get_peak_usage()));
?>
</div>

</body>
</html>