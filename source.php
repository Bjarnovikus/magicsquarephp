<?php
include("mainfunctions.php");
include("functions.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Magisch Vierkant</title>
<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
</head>
<body>

<div id="code">

<?php

	if (!(isset($_GET["method"])))
	{
		err("Er is geen methode opgegeven.");
	}
	else
	{

		$method = $_GET["method"];
		
		echo("<p><b>".methodname($method).":</b></p>");
		
		$methodfound = false;
		
		if ($method == "methodevanboogmans")
		{
			$methodfound = true;
			highlight_file("methodes/methodevanboogmans.php");
		}
		
		if ($method == "methodevandelahire")
		{
			$methodfound = true;
			highlight_file("methodes/methodevandelahire.php");
		}
		
		if ($method == "viervouden_1")
		{
			$methodfound = true;
			highlight_file("methodes/viervouden_1.php");
		}
		
		if ($method == "viervouden_2")
		{
			$methodfound = true;
			highlight_file("methodes/viervouden_2.php");
		}
		
		if ($method == "evenmethode")
		{
			$methodfound = true;
			highlight_file("methodes/evenmethode.php");
		}
		
		if ($method == "evenmethode_stap1")
		{
			$methodfound = true;
			highlight_file("methodes/evenmethode_stap1.php");
		}
		
		if ($method == "getall")
		{
			$methodfound = true;
			highlight_file("methodes/getall.php");
		}
		
		if ($methodfound == false)
		{
			err("De opgevraagde methode kon niet gevonden worden.");
		}
		
	}

?>

</div>

<div id="sourcecomment">

Bepaalde functies zoals <span class="tt">err()</span> en <span class="tt">not()</span> worden eerder gedefineerd. De overige stukken van de code zijn onafhankelijk bruikbaar zonder enige ander systeem. Bepaalde methodes maken gebruik van andere methodes, deze worden door een ander script eerder gedefineerd.<br><br>

Gebruik van deze code valt onder deze licentie:<br>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">Magisch vierkant generator</span> van <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.bjarno.be/" property="cc:attributionName" rel="cc:attributionURL">Bjarno Oeyen</a> is in licentie gegeven volgens een <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.nl">Creative Commons Naamsvermelding-GelijkDelen 3.0 Unported licentie</a>.<br>
Ik zou het appreci�ren als je <a href="http://bjarno.be/contact">mij een mailtje stuurt</a> als je deze code ergens anders gebruikt, dan ben ik op de hoogte hoe de code gebruikt wordt.

<br><br>

&copy; Bjarno.be 2013

</div>

</body>
</html>