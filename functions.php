<?php

function calcinfo($m,$n)
{

	echo("<b>Methode:</b> ".methodname($m));
	echo("<br>");
	echo("<b>n:</b> ".$n."<br>");
	$mn = magischgetal($n);
	echo("<b>Magisch getal: </b>".$mn."<br>");

}

function methodname($m)
{
	switch ($m)
	{
		case "methodevanboogmans": 		return "Methode met diagonale trappen"; break;
		case "methodevandelahire": 		return "Methode van de la Hire"; break;
		case "viervouden_1":	 		return "Methode voor viervouden (1)"; break;
		case "viervouden_2":	 		return "Methode voor viervouden (2)"; break;
		case "evenmethode":	 			return "Methode van Strachey"; break;
		case "evenmethode_stap1":		return "Methode van Strachey (ENKEL STAP 1!)"; break;
		case "getall":					return "De allesoplosser"; break;
		default:						return "-"; break;
	}
}

function magischgetal($n)
{
	
	return ($n*(1+($n*$n)))/2;
	
}

function tekenvierkant($square)
{

	if ( ($square == NULL) or (gettype($square) != "array") )
	{
		err("Er trad een fout op bij het genereren van het magisch vierkant.");
	}
	else
	{
		
		$n = count($square);
		
		echo("<table>");
		for ($yy = 0; $yy < $n; $yy++)
		{
			echo("<tr>");
			for ($xx = 0; $xx < $n; $xx++)
			{
			
				echo("<td>".$square[$xx][$yy]."</td>");
			
			}
			echo("</tr>");
		}
		echo("</table>");

	}

}

function vierkantnakijken($square)
{
	if ( ($square == NULL) or (gettype($square) != "array") )
	{
		return -1;
	}

	$n = count($square);
	$correct = true;
	$magischgetal = magischgetal($n);

	//Horizontalen
	for ($yy = 0; $yy < $n; $yy++)
	{
		$sum = 0;
		$sumstr = "";
		
		for ($xx = 0; $xx < $n; $xx++)
		{
			if ($sumstr == "")
			{
				$sumstr .= $square[$xx][$yy];
			}
			else
			{
				$sumstr .= " + ".$square[$xx][$yy];
			}
			
			$sum = $sum+$square[$xx][$yy];
		}
		
		if ($sum != $magischgetal)
		{
			$correct = false;
		}
		
		$sumstr .= " = ".$sum;
		$sums_hor[] = $sumstr;
	}

	//Verticalen
	for ($xx = 0; $xx < $n; $xx++)
	{
		$sum = 0;
		$sumstr = "";
		
		for ($yy = 0; $yy < $n; $yy++)
		{
			if ($sumstr == "")
			{
				$sumstr .= $square[$xx][$yy];
			}
			else
			{
				$sumstr .= " + ".$square[$xx][$yy];
			}
			
			$sum = $sum+$square[$xx][$yy];
		}
		
		if ($sum != $magischgetal)
		{
			$correct = false;
		}
		
		$sumstr .= " = ".$sum;
		$sums_vert[] = $sumstr;
	}

	//Diagonalen
	$sum = 0;
	$sumstr = "";

	for ($i = 0; $i < $n; $i++)
	{
		if ($sumstr == "")
		{
			$sumstr .= $square[$i][$i];
		}
		else
		{
			$sumstr .= " + ".$square[$i][$i];
		}
		
		$sum = $sum+$square[$i][$i];
	}

	if ($sum != $magischgetal)
	{
		$correct = false;
	}

	$sumstr .= " = ".$sum;
	$sum_diagonal1 = $sumstr;

	$sum = 0;
	$sumstr = "";

	for ($i = 0; $i < $n; $i++)
	{
		if ($sumstr == "")
		{
			$sumstr .= $square[$i][$n-1-$i];
		}
		else
		{
			$sumstr .= " + ".$square[$i][$n-1-$i];
		}
		
		$sum = $sum+$square[$i][$n-1-$i];
	}

	if ($sum != $magischgetal)
	{
		$correct = false;
	}

	$sumstr .= " = ".$sum;
	$sum_diagonal2 = $sumstr;

	unset($sum);
	unset($sumstr);

	//Bij foutief een waarschuwing geven
	if ($correct == true)
	{
		okm("Alle sommen gaven het juiste resultaat.");
	}
	else
	{
		err("De sommen zijn niet volledig juist! Bekijk hieronder de resultaten.");
	}

	//Op scherm printen
	echo("<table style=\"margin-top: 25px;\"><tr><td class=\"trtop\">Horizontaalsommen:</td><td class=\"trtop\">Verticaalsommen:</td></tr>");

	for ($i = 0; $i < $n; $i++)
	{
		echo("<tr><td class=\"trbodysmall\">");
		echo($sums_hor[$i]);
		echo("</td><td class=\"trbodysmall\">");
		echo($sums_vert[$i]);
		echo("</td></tr>");
	}

	echo("</table>");


	echo("<table style=\"margin-top: 25px;\"><tr><td class=\"trtop\">Diagonaalsommen:</td></tr>");

	echo("<tr><td class=\"trbodysmall\">".$sum_diagonal1."</td></tr>");
	echo("<tr><td class=\"trbodysmall\">".$sum_diagonal2."</td></tr>");

	echo("</table>");
}

?>