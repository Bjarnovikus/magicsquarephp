<?php

//Methode van de La Hire
//Methode die gebruikt wordt voor magische vierkanten met eender welke dimensie, zonder dat de diagonaallijnen kloppen

function methodevandelahire($n)
{

	if ($n <= 0)
	{
		err("Methode van de la Hire vereist een niet-negatief getal. Een negatief getal werd meegegeven.");
		return NULL;
	}
	
	if ($n == 2)
	{
		not("Ondanks dat de Methode van de la Hire een 2-magisch vierkant kan berekenen, komt deze niet overeen met de definitie van een magisch vierkant. Merk op dat er geen enkel 2-magisch vierkant bestaat dat aan de strikste definitie voldoet, ongeacht de gebruikte methode.");
	}
	else
	{
		if ($n % 2 == 0)
		{
			not("De methode van de la Hire kan een magisch vierkant met een even orde berekenen, maar gebruikt daarbij niet alle getallen van 1 tot n&sup2;.");
		}
	}
	
	for ($yy = 0; $yy < $n; $yy++)
	{
		$a = $yy+1;
		for ($xx = 0; $xx < $n; $xx++)
		{
			$square[$xx][$yy] = $a;
			$a++;
			
			if ($a > $n)
			{
				$a = 1;
			}
		}
	}
	
	
	for ($yy = 0; $yy < $n; $yy++)
	{
		$a = $yy;
		for ($xx = $n-1; $xx >= 0; $xx--)
		{
			$square[$xx][$yy] += $n*$a;
			$a++;
			
			if ($a >= $n)
			{
				$a = 0;
			}
		}
	}
	
	return $square;

}
?>