<?php

//Methode van diagonalen
//Methode die gebruikt wordt voor oneven magische vierkanten

function diagonaalmethode($n)
{
	if ($n % 2 == 0)
	{
		err("Methode met diagonalen trappen vereist een oneven getal. Een even getal werd meegegeven.");
		return NULL;
	}
	
	if ($n <= 0)
	{
		err("Methode met diagonalen trappen vereist een niet-negatief getal. Een negatief getal werd meegegeven.");
		return NULL;
	}
	
	//Bereken de start x en y
	$startxx = ($n-1)/2;
	$startyy = -($n-1)/2;
	
	$xx = $startxx;
	$yy = $startyy;
	
	//Begingetal
	$getal = 1;
	
	//We gaan elk rijtje af
	for ($i = 0; $i < $n; $i++)
	{
	
		//We gaan elk vakje af
		for ($i2 = 0; $i2 < $n; $i2++)
		{
		
			$placed = false;
			
			//D1
			if ($yy < 0)
			{
				$placed = true;
				$square[$xx][$yy+$n] = $getal;
			}
			
			//D2
			if ($xx > $n-1)
			{
				$placed = true;
				$square[$xx-$n][$yy] = $getal;
			}
			
			//D3
			if ($xx < 0)
			{
				$placed = true;
				$square[$xx+$n][$yy] = $getal;
			}
			
			//D4
			if ($yy > $n-1)
			{
				$placed = true;
				$square[$xx][$yy-$n] = $getal;
			}
			
			//DC
			if ($placed == false)
			{
				$square[$xx][$yy] = $getal;
			}
			
			$getal++;
			$xx++;
			$yy++;
			
			unset($placed);
		
		}
		
		//We resetten de lijn
		$xx = $startxx;
		$yy = $startyy;
		
		$xx--;
		$yy++;
		
		$startxx = $xx;
		$startyy = $yy;
	}
	
	return $square;

}

?>