<?php

//Methode met viervouden
//Alternatief: 1234

function viervouden1($n)
{

	if ($n <= 0)
	{
		err("Methode met viervouden vereist een niet-negatief getal. Een negatief getal werd meegegeven.");
		return NULL;
	}
	
	if ($n % 4 != 0)
	{
		err("Methode met viervouden vereist een viervoud. Een getal dat geen veelvoud is van vier werd gegeven.");
		return NULL;
	}
	
	
	$getal1 = 1;
	$getal2 = $n*$n;
	
	//0,1 => vierkant1
	//2,3 => vierkant2
	$rijstart = 0;
	
	for ($yy = 0; $yy < $n; $yy++)
	{
		if ( ($rijstart == 0) or ($rijstart == 1) )
		{
			$onoff = 0;
		}
		else
		{
			$onoff = 2;
		}
		
		for ($xx = 0; $xx < $n; $xx++)
		{
			
			if ( ($onoff == 0) or ($onoff == 1) )
			{
				$square[$xx][$yy] = $getal1;
			}
			else
			{
				$square[$xx][$yy] = $getal2;
			}
			
			$onoff--;
			
			if ($onoff == -1)
			{
				$onoff = 3;
			}
			
			$getal1++;
			$getal2--;
		}
		
		$rijstart--;
		if ($rijstart == -1)
		{
			$rijstart = 3;
		}
	}
	
	
	return $square;

}