<?php

//Alle andere methodes inladen

include_once("methodes/evenmethode.php");
include_once("methodes/methodevanboogmans.php");
include_once("methodes/viervouden_1.php");

function getallsquare($n)
{

	//Bepaalde gevallen
	if ($n == 1)
	{
		$square[0][0] = 1;
		return $square;
	}
	
	if ($n == 2)
	{
		not("Magische vierkanten van 2 bestaan niet!");
		$square[0][0] = 2;
		$square[1][0] = 3;
		$square[0][1] = 3;
		$square[1][1] = 2;
		return $square;
	}
	
	//Oneven
	if ($n % 2 == 1)
	{
		return diagonaalmethode($n);
	}
	else
	{
		//Even
		
		//Deelbaar door 4
		if ($n % 4 == 0)
		{
			return viervouden1($n);
		}
		else
		{
			//Niet deelbaar door 4
			return methodevanstrachey($n);
		}
	}

}

?>