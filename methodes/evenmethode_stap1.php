<?php

//Methode van strachey (alleen stap 1)
//Voor bekijkredenen

include_once("methodes/getall.php");

function methodevanstrachey_stap1($n)
{

	if ($n % 2 == 1)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een even getal. Een oneven getal werd meegegeven.");
		return NULL;
	}
	
	if ($n % 4 == 0)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een getal dat niet deelbaar is door 4.");
		return NULL;
	}
	
	if ($n <= 0)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een niet-negatief getal. Een negatief getal werd meegegeven.");
		return NULL;
	}
	
	not("Dit vierkant is GEEN magisch vierkant, maar laat het resultaat na de eerste stap van de methode van Strachey zien.");
	
	/// STAP 1
	
	//Linkerbovenhoek ophalen
	$parentsquare = getallsquare($n/2);
	
	//Invullen van de linkerbovenhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x][$y] = $parentsquare[$x][$y]+0;
		}
	}
	
	//Invullen van de rechterbovenhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x+($n/2)][$y] = $parentsquare[$x][$y]+(1/2)*$n*$n;
		}
	}
	
	//Invullen van de linkeronderhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x][$y+($n/2)] = $parentsquare[$x][$y]+(3/4)*$n*$n;
		}
	}
	
	//Invullen van de rechteronderhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x+($n/2)][$y+($n/2)] = $parentsquare[$x][$y]+(1/4)*$n*$n;
		}
	}
	
	return $square;
}

?>