<?php

//Methode van Strachey
//Algemene methode voor even magische vierkanten met dimensie die niet deelbaar is door 4

include_once("methodes/getall.php");

function methodevanstrachey($n)
{

	if ($n % 2 == 1)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een even getal. Een oneven getal werd meegegeven.");
		return NULL;
	}
	
	if ($n % 4 == 0)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een getal dat niet deelbaar is door 4.");
		return NULL;
	}
	
	if ($n <= 0)
	{
		err("Methode voor even dimensies niet deelbaar door 4 vereist een niet-negatief getal. Een negatief getal werd meegegeven.");
		return NULL;
	}
	
	/// STAP 1
	
	//Linkerbovenhoek ophalen
	$parentsquare = getallsquare($n/2);
	
	//Invullen van de linkerbovenhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x][$y] = $parentsquare[$x][$y]+0;
		}
	}
	
	//Invullen van de rechterbovenhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x+($n/2)][$y] = $parentsquare[$x][$y]+(1/2)*$n*$n;
		}
	}
	
	//Invullen van de linkeronderhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x][$y+($n/2)] = $parentsquare[$x][$y]+(3/4)*$n*$n;
		}
	}
	
	//Invullen van de rechteronderhoek
	for ($x = 0; $x < $n/2; $x++)
	{
		for ($y = 0; $y < $n/2; $y++)
		{
			$square[$x+($n/2)][$y+($n/2)] = $parentsquare[$x][$y]+(1/4)*$n*$n;
		}
	}
	
	//return $square;
	
	/// STAP 2
	
	//Verbeteren van de linkerkant
	$t = floor($n/4);
	
	for ($i = 0; $i < $n/2; $i++)
	{
		
		
		//Apart voor middelste rij
		if ($i == ((($n/2)+1)/2)-1)
		{
			for ($c = 1; $c < $t+1; $c++)
			{
				$temp = $square[$c][($n/2)+$i];
				$square[$c][($n/2)+$i] = $square[$c][$i];
				$square[$c][$i] = $temp;
				unset($temp);
			}
		}
		else
		{
			for ($c = 0; $c < $t; $c++)
			{
				$temp = $square[$c][($n/2)+$i];
				$square[$c][($n/2)+$i] = $square[$c][$i];
				$square[$c][$i] = $temp;
				unset($temp);
			}
		}
	}
	
	
	//Verbeteren aan de rechterkant
	
	$t--;
	
	for ($i = 0; $i < $n/2; $i++)
	{
		for ($c = 0; $c < $t; $c++)
		{
			$temp = $square[$n-1-$c][($n/2)+$i];
			$square[$n-1-$c][($n/2)+$i] = $square[$n-1-$c][$i];
			$square[$n-1-$c][$i] = $temp;
			unset($temp);
		}
	}
	
	
	return $square;
}

?>