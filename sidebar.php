<?php
include("mainfunctions.php");
?>
<?php

$methods = array(

	array(	"name" => "Methode van de la Hire",
			"callname" => "methodevandelahire",
			"description" => "Methode gebruikt voor magische vierkanten van elke oneven orde, zonder dat de diagonaallijnen kloppen.<br>Bij een even orde is er een juist magisch vierkant, maar niet alle getallen van 1 tot n� worden gebruikt."
		),
		
	array(	"name" => "Methode met diagonale trappen",
			"callname" => "methodevanboogmans",
			"description" => "Methode die gebruikt kan worden voor oneven magische vierkanten."
		),
	
	array(	"name" => "Methode voor viervouden (1)",
			"callname" => "viervouden_1",
			"description" => "Methode die gebruikt kan worden bij voor even magische vierkanten die een veelvoud zijn van vier.<br><b>Methode:</b> 1 staat linksboven."
		),
	
	array(	"name" => "Methode voor viervouden (2)",
			"callname" => "viervouden_2",
			"description" => "Methode die gebruikt kan worden bij voor even magische vierkanten die een veelvoud zijn van vier.<br><b>Methode:</b> n&sup2; staat linksboven."
		),
	
	array(	"name" => "Methode van Strachey (stap 1)",
			"callname" => "evenmethode_stap1",
			"description" => "Algemene methode voor even magische vierkanten met dimensie die niet deelbaar is door 4. Laat alleen de eerste stap zien."
		),
		
	array(	"name" => "Methode van Strachey",
			"callname" => "evenmethode",
			"description" => "Algemene methode voor even magische vierkanten met dimensie die niet deelbaar is door 4."
		),
		
	array(	"name" => "De allesoplosser",
			"callname" => "getall",
			"description" => "Script dat zelf de juiste methode kiest om een magisch vierkant op te lossen."
		),

);

?>
<?php

$_method = "";
$_n = 5;
$_checksquare = false;

if (isset($_POST["method"]))
{
	$_method = $_POST["method"];
}

if (isset($_POST["n"]))
{
	$_n = floatval($_POST["n"]);
}

if (isset($_POST["checksquare"]))
{
	$_checksquare = true;
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Select a method</title>
<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
<script language="JavaScript" type="text/javascript">
function changedescription(type)
{
	document.getElementById("descriptionbox").innerHTML = "";
	document.getElementById("descriptionbox").style.display = "none";
	
	<?php
		
		foreach ($methods as $method)
		{
		
			?>
			if (type == "<?php echo($method["callname"]); ?>")
			{
				document.getElementById("descriptionbox").innerHTML = "<?php echo($method["description"]); ?><br><br><?php
				
				echo("<a href=\\\"source.php?method=".$method["callname"]."\\\" target=\\\"mainwindow\\\">Broncode</a>");
				
				?>";
				document.getElementById("descriptionbox").style.display = "block";
			}
			<?php
		
		}
		
	?>

}
</script>
</head>
<body>
	
	<?php
	if ($_method != "")
	{
		?>
		<script language="JavaScript" type="text/javascript">
		
		top.frames["mainwindow"].location = "display.php?n=<?php echo($_n); ?>&method=<?php echo($_method); if (isset($_POST["checksquare"])) {echo("&checksquare"); } ?>";
		
		</script>
		<?php
	}
	?>
	
	<b>Instellingen:</b>
	
	<form method="post" action="sidebar.php">
	
		<select name="method" onchange="changedescription(this.value);">
		<option value="none" disabled<?php if ($_method == "") { echo(" selected"); } ?>>Selecteer een methode</option>
		<?php
		
			foreach ($methods as $method)
			{
			
				echo("<option value=\"".$method["callname"]."\"");
				
				if ($_method == $method["callname"])
				{
					echo(" selected");
				}
				
				echo(">".$method["name"]."</option>");
			
			}
		
		?>
		</select><br>
		
		n: <input type="text" name="n" value="<?php echo($_n); ?>"><br>
		
		<label for="checksquare">
		<input <?php if ($_checksquare == true) { echo("checked "); } ?>type="checkbox" name="checksquare" id="checksquare"> Reken vierkant na</label>
		<a href="checksquare.html" target="mainwindow">[?]</a><br>
		
		
		<input type="submit" value="Bereken">
	
	</form>
	
	<?php
	if ($_method == "")
	{
	?>
	<div id="descriptionbox" style="display: none;">
	
	</div>
	<?php
	}
	else
	{
	?>
	<div id="descriptionbox" style="display: block;">
	
		<?php
		
			foreach ($methods as $method)
			{
			
				if ($method["callname"] == $_method)
				{
					echo($method["description"]);
					echo("<br><br>");
					echo("<a href=\"source.php?method=".$method["callname"]."\" target=\"mainwindow\">Broncode</a>");
				}
			
			}
		
		?>
	
	</div>
	<?php
	}
	?>

</body>
</html>